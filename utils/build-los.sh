#!/bin/bash

# By @fremontt
# This file might contain long lines, so make sure to enable "word wrap" in your text editor

# Constants
vendor="lineage" # Taken from original repo
SIGSPOOF_PATCHES_DIR="${BUILD_ROOT}/patches/signature_spoofing_patches"

function printMessage
{
    NOW=$(date +"%Y-%m-%d %T")
    echo "[$NOW] ${1}"
}
function finishError
{
    printMessage "Error: ${1}"
    exit 1
}
function preInit
{
    # Change dir to main build files
    cd "${BUILD_ROOT}" || finishError "Error when cd to main build folder"
}
function welcome
{
    printMessage "Running the Minimal LineageOS + MicroG compilation with the following parameters:"
    echo -e "\tVendor: ${DEVICE_VENDOR}"
    echo -e "\tDevice: ${DEVICE_CODENAME}"
    echo -e "\tSignature spoofing: ${LOS_SIGSPOOF_TYPE}"
    echo -e "\tAdditional packages: (${LOS_ENABLE_CUSTOM_PACKAGES}) ${LOS_CUSTOM_PACKAGES}"
    echo -e "\tUse ccache?: ${USE_CCACHE}"
    echo -e "\tDir ccache: ${CCACHE_DIR}"
    echo -e "\tUse compressed ccache?: ${CCACHE_NOCOMPRESS}"
    echo -e "\tJava VM args: ${JAVA_TOOL_OPTIONS}"
}
function preBuild
{
    printMessage "Doing some preparations previous to build process"

    # Configure Git variables used by git-repo tool
    git config --global user.name "${GIT_USERNAME}" || finishError "Could not setup Git username"
    git config --global user.name "${GIT_EMAIL}" || finishError "Could not setup Git email"

    # Setup ccache
    ccache -M $CCACHE_SIZE || finishError "Could not setup ccache size"

    # Check existing build keys
    for KEY in releasekey platform shared media networkstack testkey
    do
        if [ ! -f "./custom_keys/${KEY}.pk8" ] || [ ! -f "./custom_keys/${KEY}.x509.pem" ]
        then
            finishError "${KEY}.pk8/${KEY}.x509.pem were not found in 'custom_keys' folder"
        fi
    done

    # Cleaning the patched files created in previous builds, as this might cause issues on already patched files
    if [ -d "$BUILD_ROOT/frameworks/base" ]
    then
        rm -rf $BUILD_ROOT/frameworks/base || finishError "Could not delete old sigspoof patched files"
    fi
    # Cleaning the MicroG modified file several times to avoid repeating lines when "sed"
    if [ -f "$BUILD_ROOT/vendor/$vendor/config/common.mk" ]
    then
        rm -f "$BUILD_ROOT/vendor/$vendor/config/common.mk"
    fi
}
function configRepo
{
    printMessage "Config repo steps"

    if [ ! -d "./.repo" ]
    then
        printMessage "Init git-repo in $(pwd)"
        repo init -u https://github.com/LineageOS/android.git -b "${LOS_BRANCH}" || finishError "Could not init repo"
    fi

    if [ ! -d "./custom_manifests" ]
    then
        finishError "No custom manifests folder 'custom_manifests' is available"
    fi

    # Copy the custom manifests so "repo" can fetch our custom sources, like vendor or MicroG binaries. DON'T use symlinks (sometimes there are weird bugs when trying to create them)
    if [ -d "$BUILD_ROOT/.repo/local_manifests" ]
    then
        rm -rf $BUILD_ROOT/.repo/local_manifests || finishError "Could not delete old copies of local manifests"
    fi
    mkdir $BUILD_ROOT/.repo/local_manifests || finishError "Error when creating a dir for repo local manifests"
    cp -f $BUILD_ROOT/custom_manifests/* $BUILD_ROOT/.repo/local_manifests/ || finishError "Could not create a copy for local manifests"
}
function syncSources
{
    printMessage "Syncing sources"
    repo sync --verbose || finishError "Could not sync source files"
}
function overlays
{
    # I need to understand what is the reason of these lines: https://github.com/SolidHal/docker-lineage-cicd/blob/lineageos-17.1/src/build.sh#L177

    if [ "$LOS_ENABLE_CUSTOM_PACKAGES" = "YES" ]
    then
        mkdir -p "vendor/$vendor/overlay/microg/" || finishError "Could not set up overlays"
        sed -i "1s;^;PRODUCT_PACKAGE_OVERLAYS := vendor/$vendor/overlay/microg\n;" "vendor/$vendor/config/common.mk" || finishError "Could not set up overlays"
    fi
}
function applySigPatch
{
    cd frameworks/base || finishError "Directory $(pwd)/frameworks/base does not exist"
    case $LOS_SIGSPOOF_TYPE in
        "FULL")
            patch --quiet --suffix=".patch-desechable" -p1 -i "${SIGSPOOF_PATCHES_DIR}/android_frameworks_base-Q.patch" || finishError "Could not apply the patch"
            ;;
        "RESTRICTED")
            sed 's/android:protectionLevel="dangerous"/android:protectionLevel="signature|privileged"/' "${SIGSPOOF_PATCHES_DIR}/android_frameworks_base-Q.patch" | patch --quiet --suffix=".patch-desechable" -p1 || finishError "Could not apply the patch"
            ;;
    esac
    find . -iname "*.patch-desechable" -delete
    cd ../..
}
function signatureSpoofing
{
    if [ "$LOS_SIGSPOOF_TYPE" = "FULL" ] || [ "$LOS_SIGSPOOF_TYPE" = "RESTRICTED" ]
    then
        printMessage "Apply patch to enable ${LOS_SIGSPOOF_TYPE} signature spoof"
        applySigPatch
        mkdir -p "vendor/$vendor/overlay/microg/frameworks/base/core/res/res/values/"
        cp -f "${SIGSPOOF_PATCHES_DIR}/frameworks_base_config.xml" "vendor/$vendor/overlay/microg/frameworks/base/core/res/res/values/config.xml" || finishError "Could not copy frameworks_base XML"
    else
        printMessage "Signature spoof won\'t be enabled"
    fi
}
function defineCustomPackages
{
    if [ "$LOS_ENABLE_CUSTOM_PACKAGES" = "YES" ]
    then
        printMessage "Define custom packages to be integrated on the system"
        sed -i "1s;^;PRODUCT_PACKAGES += $LOS_CUSTOM_PACKAGES\n\n;" "vendor/$vendor/config/common.mk" || finishError "Cannot apply replacements for custom packages"
    fi
}
function build
{
    printMessage "Starting LOS build"

    source build/envsetup.sh || finishError "Error when sourcing envsetup.sh"
    breakfast "${DEVICE_CODENAME}" || finishError "A problem happened on the breakfast"

    # This tries to solve the problem "Can not locate config makefile for product 'lineage_lake'" when running breakfast $DEVICE_CODENAME
    # The device tree (in my case located at $BUILD_ROOT/device/motorola/lake) has a file called "AndroidProducts.mk", where there is a variable "PRODUCT_MAKEFILES" that says there should be a file in $BUILD_ROOT called "lineage_lake.mk"
    # For some reason, that file does not exist, but apparently is the same I have in my device tree folder, so I will simply copy it to the workdir
    # But we need the device tree first, so let's breakfast do that job
    if ! [ -f "$BUILD_ROOT/${vendor}_$DEVICE_CODENAME.mk" ]
    then
        cp $BUILD_ROOT/device/$DEVICE_VENDOR/$DEVICE_CODENAME/${vendor}_$DEVICE_CODENAME.mk $BUILD_ROOT/ || finishError "Could not copy ${vendor}_$DEVICE_CODENAME.mk to fix issues with breakfast"
    fi
    
    overlays
    signatureSpoofing
    defineCustomPackages

    mka target-files-package otatools || finishError "Build error with mka"
}
function signBuild
{
    printMessage "Signing output builds"
    cd $BUILD_ROOT

    ./build/tools/releasetools/sign_target_files_apks -o -d $BUILD_ROOT/custom_keys $OUT/obj/PACKAGING/target_files_intermediates/*-target_files-*.zip signed-target_files.zip || finishError "Error ocurred when signing APKs"

    ./build/tools/releasetools/ota_from_target_files -k $BUILD_ROOT/custom_keys/releasekey --block --backup=true signed-target_files.zip signed-ota_update.zip || finishError "Error ocurred when signing the generated build"
}
function saveOutputs
{
    printMessage "Renaming output builds"

    cd $BUILD_ROOT
    TIMESTAMP=$(date +"%Y%m%d")
    find "$BUILD_ROOT/out/target/product/$DEVICE_CODENAME" -maxdepth 1 -name "boot.img" -exec mv -t "$BUILD_ROOT" {} + || finishError "Could not find recovery"
    mv boot.img "${LOS_BRANCH}-${TIMESTAMP}-recovery-${DEVICE_CODENAME}.img" || finishError "Could not rename recovery"
    mv signed-ota_update.zip "${LOS_BRANCH}-${TIMESTAMP}-minimal-${DEVICE_CODENAME}-signed.zip" || finishError "Signed ZIP build not found"
    rm -f signed-target_files.zip
}
function init
{
    preInit
    welcome
    preBuild
    configRepo
    syncSources
    build
    signBuild
    saveOutputs
    printMessage "Task finished"
    exit
}

init

# "Que chingue a su madre Google...¡y que la vuelva a chingar!" - Cadetes de Linares ft. Frmt
