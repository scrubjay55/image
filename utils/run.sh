#!/bin/bash

# /usr/bin/time is a better approach that allows more format options than shell-builtins "time" keyword
/usr/bin/time --format="Total elapsed execution time: %E" /bin/bash ./build-los.sh
exit