# Minimal Docker for LOS

Build LineageOS + MicroG (optional) from a minimal Docker container.

[[_TOC_]]

## Getting started

> You can run your own build on DigitalOcean with [Infra](https://gitlab.com/fremontt/minimal-docker-for-lineageos/infra), just **[create a new account and get $100 USD free in credits](https://m.do.co/c/397805aa0e0e)**

Please take in account `$YOUR_PROJECT_FOLDER` is a location on your computer (or cloud instance) where all the needed build files will be stored (more than 200GB), including your signature keys and output builds.

## Setup

1. Make sure you have at least 250GB free on your disk. Then create some needed folders into `$YOUR_PROJECT_FOLDER`.

`cd $YOUR_PROJECT_PATH && mkdir -p {ccache,custom_keys,custom_manifests}`

2. Copy this XML to `$YOUR_PROJECT_PATH/custom_manifests/minimal-docker-los.xml`, replacing your vendor

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="themuppetsGithub" fetch="https://github.com/TheMuppets/" revision="lineage-17.1" />
    <remote name="themuppetsGitlab" fetch="https://gitlab.com/the-muppets/" revision="lineage-17.1" />
    <remote name="omniromGithub" fetch="https://github.com/omnirom/" />
    <remote name="minimalLOSGitlab" fetch="https://gitlab.com/fremontt/minimal-docker-for-lineageos/" />

    <project name="android_prebuilts_prebuiltapks" path="packages/apps/prebuilt" remote="omniromGithub" revision="android-10" />
    <project name="android_sigspoof_patches" path="patches" remote="minimalLOSGitlab" revision="lineage-17.1" />

    <project name="proprietary_vendor_motorola" path="vendor/motorola" remote="themuppetsGithub" />
</manifest>
```

> This XML is a Manifest file to define all additional sources to build LOS with MicroG and your vendor/device specific binaries. Please check [the docs](https://gerrit.googlesource.com/git-repo/+/master/docs/manifest-format.md) for more info.

> LPT: Avoid putting comments into the manifests file or you will suffer a hell of random errors

3. Generate and copy your signature keys into `$YOUR_PROJECT_FOLDER/custom_keys`. Use [this script](https://github.com/LineageOS/android_development/blob/lineage-17.1/tools/make_key) to generate them and [follow the instructions of LineageOS](https://wiki.lineageos.org/signing_builds.html).

> You only need to generate your build keys once, on later builds simply copy your existing files to the folder. If you lose your keys, you can generate new ones, but you will need to do a clean install of the new build.

## Running build

Running the build should be as easy as execute a couple of commands.

1. Build the image: `docker build -t fremontt/losbuilder https://gitlab.com/fremontt/minimal-docker-for-lineageos/image.git`

2. Run a new container that will build your LOS image automatically: `docker run -v $YOUR_PROJECT_FOLDER:/root/los-build fremontt/losbuilder`

3. When the build finishes, you should check the zip files at `$YOUR_PROJECT_FOLDER/lineage-*.zip`

Please check out the examples before doing anything.

### Examples

Remember to ALWAYS build the Docker image before running. In the examples I assume you already ran the command (1) and understand the basics of Docker.

#### Building for Moto G7 Plus (lake): default of this project

`docker run -v $YOUR_PROJECT_FOLDER:/root/los-build fremontt/losbuilder`

#### Building for lake with the full log output on a file

`docker run -v $YOUR_PROJECT_FOLDER:/root/los-build fremontt/losbuilder 2>&1 | tee /home/$YOUR_USER/los-microg-build.log`

#### Building for lake + FULL signature spoofing (all installed apps could have the sigspoof permission)

`docker run -v $YOUR_PROJECT_FOLDER:/root/los-build -e LOS_SIGSPOOF_TYPE="FULL" fremontt/losbuilder`

#### Building for lake + RESTRICTED signature spoofing (only system apps can have the sigspoof permission)

`docker run -v $YOUR_PROJECT_FOLDER:/root/los-build -e LOS_SIGSPOOF_TYPE="RESTRICTED" fremontt/losbuilder`

#### Building for lake + RESTRICTED signature spoofing + [custom packages](https://github.com/omnirom/android_prebuilts_prebuiltapks)

`docker run -v $YOUR_PROJECT_FOLDER:/root/los-build -e LOS_SIGSPOOF_TYPE="RESTRICTED" -e LOS_ENABLE_CUSTOM_PACKAGES="YES" -e LOS_CUSTOM_PACKAGES="GmsCore GsfProxy FakeStore MozillaNlpBackend com.google.android.maps" fremontt/losbuilder`

#### Building for another supported device: [Xiaomi "lavender"](https://wiki.lineageos.org/devices/lavender/)

> Note: I haven't tested this because I'm not a "lavender" device owner; is just an example.

**Manifest file**

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    ...
    <project name="proprietary_vendor_xiaomi" path="vendor/xiaomi" remote="themuppetsGitlab" />
    ...
</manifest>
```

`docker run -v $YOUR_PROJECT_FOLDER:/root/los-build -e DEVICE_VENDOR="xiaomi" -e DEVICE_CODENAME="lavender" fremontt/losbuilder`

## FAQ

### The build integrates UnifiedNLP?

No. I will need your help to integrate UnifiedNLP on this project once it runs with LineageOS 17.1, as the Omnirom prebuilts do not include it.

### Are you going to integrate FDroid, Aurora and additional packages to the build?

No, but you can define them in the `LOS_CUSTOM_PACKAGES` variable, assuming all the packages you want to add are defined on the "prebuilt_apks" repo.

In fact I prefer the approach of having FDroid, Aurora, etc. installed as user-apps.

### If I disable all features related to MicroG, what will be the final result?

The final image should be an original LineageOS 17.1 ROM, just signed with your own keys.

### I want to build LOS 17.1 for an unsupported device...

You'll need to define the kernel sources, custom vendor files and other things by yourself. You can use this project as a start point, but I don't plan to add support to build unsupported devices.

### My build fails, what can I do?

First, you need a lot of patience to dig into the problem, sometimes will be from your side and sometimes could be from the project itself. Please check carefully you are running the latest version of this Docker image, check you have all required dependencies installed, check file permissions, etc.

Happened to me the build failed several times; I solved it deleting the `vendor` and `frameworks` folder and running the build process again. Don't worry, it will download the files again.

In other cases, try to change the values of the environment variables, if you know what are you doing. For example disable signature spoofing, remove some prebuilt packages, etc.

Also, check your device pipeline on [LineageOS Build pipelines](https://buildkite.com/lineageos/). If something is failing there on your device, surelly it will fail with this Docker image, as we depend from the same sources.

Of course, you can always open a new issue.

### The build failed due to a lack of memory

I suggest you play with the `JAVA_TOOL_OPTIONS`, expanding your swapfile or running the build into a better hardware machine.

### My custom build is not working with MicroG

At the moment I'm writing this, the MicroG community is working with compatibility on Android Q (LOS 17.1). After several tests I did on my cellphone, I suggest build your custom image but leave only ENABLED the signature spoofing. Install your custom build on your phone, flash [Magisk from the official Github](https://github.com/topjohnwu/Magisk/releases) and install [MinMicroG](https://github.com/FriendlyNeighborhoodShane/MinMicroG_releases/releases) using the app called Magisk Manager (available once you flash Magisk).

### How much time does the build takes?

On my experience, the first time I run it into a DigitalOcean droplet, it can take around 9-12 hours since the deploy of the droplet and configuration.

For next builds, using existing resources like downloaded repos and ccached objects, might take around 2-3 hours.

From other side, only the Docker image takes around 25-40 minutes to be compiled.

Please take in account all times providen in this question are considered from a deployed instance on DigitalOcean, as I don't have the resources to build LOS on my computer.

### Why does the build process should run as root?

I considered running the build process (on the Archlinux Docker container) as a non-root user, but I haven't tested yet. In fact I'm sure it will cause some trouble with your keys or the manifest file, for example, when trying to access to them, considering the host user runs as root the commands to build/run the Docker container.

TL;DR: will cause a lot of troubles. For now it works as is and everything else does not contribute so much value to this project, unless it implies a big security issue.

### Are you going to replace the LineageOS recovery with TWRP?

No, I will stick to the resources LineageOS provides on their repos. Unfortunately, TWRP has several problems on Android 10 that apparently are not going to be fixed soon because the developer [is not taking care of the project at this moment](https://twrp.me/site/update/2019/10/23/twrp-and-android-10.html).

I suggest check out from time to time [the TWRP Gerrit](https://gerrit.twrp.me/), as this recovery adds so much value to LineageOS and related projects for the final user.

We hope the developers of LOS recovery add support for Nandroid backups in the meantime, but I don't think this might be a priority for them.

### This Docker image works for LineageOS 16 builds and below?

No, I'm sure it won't work. You might need to install additional packages into the Docker image. But remember you can always use this project as a start point to create derivative projects that fit your needs.

## Credits and license

Thanks to

- **MicroG** (Marvin and collaborators)
- **LineageOS For MicroG** team (they are the authors of the Docker image that was a start point for this project)
- **SolidHal** (author of patches and improvements to run the builds for LOS 17.1)
- **Omnirom** team (for the prebuilt APKs that run in Android 10)
- **TheMuppets** team (providers of vendor binaries for a lot of devices)
- **FriendlyNeighborhoodShane** and collaborators, for the MinMicroG package which is a fork of MicroG that works on Android 10
- The whole **LineageOS** team, for this fantastic project
- And the (future) contributors of **Minimal Docker for LOS**

>If part of your contributions are directly used by this project, but I didn't mention you, please let me know and I will add you to the list. Thanks for understanding and apologizes for that.

This is an open-source project by **@fremontt** under GNU GPL v3.0
