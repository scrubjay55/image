FROM archlinux:latest

LABEL maintainer="@fremontt"
LABEL projectUrl="https://gitlab.com/fremontt/minimal-docker-for-lineageos"

# Env variables needed in Docker image build; no need to be overriden
ENV BUILD_USER "builderuser"

# Creating layers for the image: we need to start from the less volatile according to https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#use-multi-stage-builds

# First steps
USER root
RUN useradd -m -G nobody $BUILD_USER \ 
    && echo -e "\n[multilib]\nInclude = /etc/pacman.d/mirrorlist\n" >> /etc/pacman.conf \
    && pacman -Syu --noconfirm base-devel git \
    && echo "$BUILD_USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# Install yay (package manager for AUR)
WORKDIR /home/$BUILD_USER
USER $BUILD_USER
RUN git clone https://aur.archlinux.org/yay-bin.git \
    && cd yay-bin \
    && makepkg -s --skippgpcheck \
    && find . -iname "*.pkg.tar.xz" -exec sudo pacman --noconfirm -U {} \; \
    && cd ..
# Install Pacman packages (according to https://wiki.archlinux.org/index.php/Android#Required_packages). As Arch is a rolling-release distro, we must avoid partial upgrades
RUN sudo pacman -Syu --noconfirm multilib-devel \
    gcc \
    repo \
    gnupg \
    gperf \
    sdl \
    wxgtk2 \
    squashfs-tools \
    curl \
    ncurses \
    zlib \
    schedtool \
    perl-switch \
    zip \
    unzip \
    libxslt \
    bc \
    rsync \
    ccache \
    lib32-zlib \
    lib32-ncurses \
    lib32-readline \
    ttf-dejavu \
    time \
    vim


# Environment variables should be near the end of the Dockerfile to avoid recompilation of upper layers

# Environment variables (you can override them when running this Docker generated image)
ENV DEVICE_VENDOR "motorola"
ENV DEVICE_CODENAME "lake"
ENV LOS_SIGSPOOF_TYPE "NO"
ENV LOS_ENABLE_CUSTOM_PACKAGES "NO"
ENV LOS_CUSTOM_PACKAGES "GmsCore GsfProxy FakeStore MozillaNlpBackend com.google.android.maps"

# Advanced environment variables
ENV BUILD_ROOT "/root/los-build"
ENV LOS_BRANCH "lineage-17.1"
ENV GIT_USERNAME "LineageOS Docker"
ENV GIT_EMAIL "microg.docker@lineageos.org"
ENV USE_CCACHE "1"
ENV CCACHE_SIZE "50G"
ENV CCACHE_DIR "$BUILD_ROOT/ccache"
ENV CCACHE_EXEC "/usr/bin/ccache"
ENV CCACHE_NOCOMPRESS "true"
ENV JAVA_TOOL_OPTIONS "-Xms1024m -Xmx8192m"
# No need to set the ANDROID_JACK_VM_ARGS variable, as Jack is deprecated: https://source.android.com/setup/build/jack

USER root
VOLUME $BUILD_ROOT
ENTRYPOINT bash ./run.sh
